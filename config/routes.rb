Rails.application.routes.draw do

  devise_for :members, path: 'members'
  resources :form_barats 
  
  root :to => "pages#index"

  get 'wilayah_barat' => 'wilayah_barats#index'
  get 'wilayah_barat/aceh' => 'wilayah_barats#aceh'
  get 'wilayah_barat/sumaterautara' => 'wilayah_barats#sumatera_utara'
  get 'wilayah_barat/riau' => 'wilayah_barats#riau'
  get 'wilayah_barat/kepulauanriau' => 'wilayah_barats#kepulauan_riau'
  get 'wilayah_barat/sumaterabarat' => 'wilayah_barats#sumatera_barat'
  get 'wilayah_barat/bengkulu' => 'wilayah_barats#bengkulu'
  get 'wilayah_barat/sumateraselatan' => 'wilayah_barats#sumatera_selatan'
  get 'wilayah_barat/lampung' => 'wilayah_barats#lampung'
  get 'wilayah_barat/banten' => 'wilayah_barats#banten'
  get 'wilayah_barat/jawatimur' => 'wilayah_barats#jawa_timur'

  get 'wilayah_barat/mydata' => 'form_barats#barat_mydata'



  resources :members do
    resources :form_barats
  end
end
